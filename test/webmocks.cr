require "webmock"

WebMock.stub(:get, "https://covidapi.info/api/v1/country/CAN").to_return(status: 200, body: File.read("test/files/canada.json"))
WebMock.stub(:get, "https://covidapi.info/api/v1/country/CAD").to_return(status: 404, body: %({"count":0,"result":{}}))

WebMock.stub(:get, "https://covidapi.info/api/v1/country/CAN/latest").to_return(status: 200, body: File.read("test/files/canada-latest.json"))

WebMock.stub(:get, "https://covidapi.info/api/v1/global").to_return(status: 200, body: File.read("test/files/global-latest.json"))

WebMock.stub(:get, "https://covidapi.info/api/v1/global/count").to_return(status: 200, body: File.read("test/files/global.json"))
